package csd203.michael.walsh.whackamole.enemy;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

import csd203.michael.walsh.whackamole.R;

public class Steves extends Enemy {

    public Steves( )
    {
        Init();


    }

    @Override
    protected void UpdateCount(int val)
    {
        SetCount(EnemyTypes.Steves, val);
    }

    @Override
    public void hit()
    {
        super.hit();
        Move();
    }



    @Override
    protected void Init()
    {
        SetSprites(Enemy.STEVES_REGULAR);
        SetHP(3);
        SetImage(GetSpriteSet().GetIdleSprite());
        SetAgility(950);
        SetSize(175);
        SetStrength(15);
        super.Init();

    }

}
