package csd203.michael.walsh.whackamole.enemy;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.media.Image;
import android.view.View;

import java.sql.Struct;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import csd203.michael.walsh.whackamole.MainActivity;
import csd203.michael.walsh.whackamole.R;
import csd203.michael.walsh.whackamole.SpriteSet;
import csd203.michael.walsh.whackamole.WhackaView;

public abstract class Enemy {
    final public static SpriteSet MIKE_REGULAR =
            new SpriteSet(R.drawable.mike, R.drawable.attackmike, R.drawable.enragedmike, R.drawable.deadguy);

    final public static SpriteSet GREAME_REGULAR =
            new SpriteSet(R.drawable.greame, R.drawable.attackgreame, R.drawable.hitgreame, R.drawable.cashmunny);

    final public static SpriteSet CONNOR_REGULAR =
            new SpriteSet(R.drawable.connor, R.drawable.attackconnor, R.drawable.hitconnor, R.drawable.catthing);

    final public static SpriteSet STEVES_REGULAR =
            new SpriteSet(R.drawable.steves, R.drawable.attacksteves, R.drawable.hitsteves, R.drawable.deadguy);

    final public static SpriteSet MIKE_ENRAGED =
            new SpriteSet(R.drawable.enragedmike, R.drawable.enragedattackmike, R.drawable.mike, R.drawable.deadguy);

    protected boolean alive = true;

    private SpriteSet set;

    static public View context;

    static private Map<EnemyTypes, Integer> EnemyCounts = new HashMap<>();

    private int pointVal = 1;

    public enum EnemyTypes{
        Steves,
        Mikes,
        Connors,
        Greames,
    }


    private int hp;

    private int xPos;
    private int yPos;

    private int size;

    private int agility;
    private int strength;

    private Bitmap image;

    protected void SetCount(EnemyTypes eType, int val)
    {
        int CurrentVal = 0;
        if(!EnemyCounts.containsKey(eType))
            EnemyCounts.put(eType, 0);
        else
            CurrentVal = EnemyCounts.get(eType);

        EnemyCounts.put(eType, CurrentVal + val);
    }

    public static void ClearCounts()
    {
        EnemyCounts.clear();
    }

    public int GetPointVal()
    {
        return pointVal;
    }

    public void SetPointVal(int val)
    {
        pointVal = val;
    }

    public void SetSprites(SpriteSet sprites)
    {
        set = sprites;
    }

    public SpriteSet GetSpriteSet() {
        return set;
    }

    public void hit()
    {
        if(!alive)
            return;
        SetImage(GetSpriteSet().GetHitSprite());
        MainActivity.instance.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                context.invalidate();
            }
        });
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if(alive)
                {
                    SetImage(GetSpriteSet().GetIdleSprite());
                    MainActivity.instance.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            context.invalidate();
                        }
                    });
                }

            }
        }, 250);

        if(!alive)
            return;
        SetHP(GetHP() - 1);

        if(GetHP() < 0)
            Kill();


    }

    public Rect GetRect()
    {
        return new Rect(xPos,yPos,xPos+ size, yPos + size);
    }

    public void SetHP(int h)
    {
        hp = h;
    }

    public int GetHP()
    {
        return hp;
    }

    public void SetPos(int x, int y)
    {
        xPos = x;
        yPos = y;
    }

    public void SetPos(int[] newPos)
    {
        xPos = newPos[0];
        yPos = newPos[1];
    }

    public void SetSize(int newSize)
    {
        size = newSize;
    }

    public int GetSize()
    {
        return size;
    }

    public void SetStrength(int newStrength)
    {
        strength = newStrength;
    }

    public int GetStrength()
    {
        return strength;
    }

    public void SetAgility(int newAgility)
    {
        agility = newAgility;
    }

    public int GetAgility()
    {
        return agility;
    }


    public Bitmap GetImage()
    {
        return image;
    }

    public void SetImage(int drawer)
    {
        Bitmap icon = BitmapFactory.decodeResource( context.getResources(), drawer);
        image = icon;
    }

    protected void Init()
    {
        UpdateCount(1);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if(alive)
                Attack();
            }
        }, agility);
    }

    protected abstract void UpdateCount(int val);

    protected int GetCount(EnemyTypes eType)
    {
        return EnemyCounts.get(eType);
    }

    protected void Attack()
    {
        if(!alive)
            return;
        SetImage(GetSpriteSet().GetAttackSprite());
        MainActivity.instance.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                context.invalidate();
            }
        });

        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                if(alive)
                {
                    SetImage(GetSpriteSet().GetIdleSprite());
                    MainActivity.instance.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            context.invalidate();
                        }
                    });
                }

            }
        }, 250);

        if(!((WhackaView) context).isAlive())
            return;
        ((WhackaView) context).Hit(strength);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if(alive)
                    Attack();
            }
        }, agility);
    }

    public void Kill()
    {
        if(!alive)
            return;
        UpdateCount(-1);
        alive = false;
        WhackaView.score += GetPointVal();
        ((WhackaView)context).activity.UpdateScoreText();
        SetImage(GetSpriteSet().GetDeadSprite());
        ((WhackaView) context).KillEnemy(this);

    }

    public boolean isAlive(){
        return alive;
    }

    public void Move()
    {
        SetPos(WhackaView.GenerateRandomCoordinates());
    }


}
