package csd203.michael.walsh.whackamole.enemy;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

import csd203.michael.walsh.whackamole.R;
import csd203.michael.walsh.whackamole.SpriteSet;
import csd203.michael.walsh.whackamole.WhackaView;

public class Mike extends Enemy {

    public Mike()
    {
        Init();

    }

    @Override
    protected void Init()
    {
        Enrage(false    );
        SetHP(1);
        SetImage(GetSpriteSet().GetIdleSprite());
        SetAgility(550);
        SetSize(185);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Kill();
            }
        }, 12000);

        super.Init();
    }

    @Override
    protected void UpdateCount(int val)
    {
        SetCount(EnemyTypes.Mikes, val);
        if(GetCount(EnemyTypes.Mikes)  > 1)
        {
            ((WhackaView)context).EnrageMikes(true);
            Enrage(true);
        }
        else if(GetCount(EnemyTypes.Mikes )<= 1)
        {
            ((WhackaView)context).EnrageMikes(false);
            Enrage(false);
        }


    }

    public void Enrage(boolean state)
    {
        if(state)
        {
            SetSprites(Enemy.MIKE_ENRAGED);
            SetStrength(10);

        }
        else
        {
            SetSprites(Enemy.MIKE_REGULAR);
            SetStrength(-15);
        }
    }

}