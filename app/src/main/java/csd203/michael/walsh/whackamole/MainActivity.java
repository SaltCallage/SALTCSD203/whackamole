package csd203.michael.walsh.whackamole;

import android.app.ActionBar;
import android.content.Context;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import csd203.michael.walsh.whackamole.enemy.Enemy;

public class MainActivity extends AppCompatActivity {
    private ProgressBar hpBar;
    private static TextView scoreText;
    private static View v;
    public static View gameOverScreen;
    ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    public static MainActivity instance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Display display = getWindowManager().getDefaultDisplay();



        Point size = new Point();
        display.getSize(size);

        int width = size.x;
        int height = size.y - 200;

        instance = this;


        WhackaView view = new WhackaView(this);
        v = view;
        Enemy.context = view;
        view.SetMaxCoords(width, height);
        setContentView(view);
        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View ui = inflater.inflate(R.layout.activity_main,null);
        addContentView(ui, params);

        gameOverScreen = inflater.inflate(R.layout.game_over,null);
        addContentView(gameOverScreen, params);
        gameOverScreen.setVisibility(View.GONE);

        hpBar = findViewById(R.id.health);
        hpBar.setMax(view.GetHP());
        hpBar.setProgress(view.GetHP());

        scoreText = findViewById(R.id.cash);

        Button restartButton = findViewById(R.id.RetryButton);
        restartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vee) {
                ((WhackaView) v).NewGame();
                gameOverScreen.setVisibility(View.GONE);
            }
        });
    }

    public void UpdateHP()
    {
        hpBar.setProgress(((WhackaView) v).GetHP());
    }

    public void UpdateScoreText()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                scoreText.setText( ""+ WhackaView.score);
            }
        });

    }

    public void GameOver()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                gameOverScreen.setVisibility(View.VISIBLE);

                TextView scoreText = findViewById(R.id.scoreText);
                scoreText.setText(""+ WhackaView.score);
            }
        });


    }

}
